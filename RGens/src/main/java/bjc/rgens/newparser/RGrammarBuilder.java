package bjc.rgens.newparser;

import bjc.utils.funcdata.FunctionalList;
import bjc.utils.funcdata.IList;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static bjc.rgens.newparser.RuleCase.CaseType.*;

/**
 * Construct randomized grammars piece by piece.
 *
 * @author EVE
 *
 */
public class RGrammarBuilder {
	private Map<String, Rule> rules;

	private Set<String> exportedRules;

	private String initialRule;

	/**
	 * Create a new randomized grammar builder.
	 */
	public RGrammarBuilder() {
		rules = new HashMap<>();

		exportedRules = new HashSet<>();
	}

	public Rule getOrCreateRule(String rName) {
		if(rName == null)
			throw new NullPointerException("Rule name must not be null");
		else if(rName.equals(""))
			throw new IllegalArgumentException("The empty string is not a valid rule name");

		if(rules.containsKey(rName))
			return rules.get(rName);
		else {
			Rule ret = new Rule(rName);

			rules.put(rName, ret);

			return ret;
		}
	}

	/**
	 * Convert this builder into a grammar.
	 *
	 * @return The grammar built by this builder
	 */
	public RGrammar toRGrammar() {
		RGrammar grammar = new RGrammar(rules);

		grammar.setInitialRule(initialRule);

		grammar.setExportedRules(exportedRules);

		return grammar;
	}

	/**
	 * Set the initial rule of the grammar.
	 *
	 * @param init
	 *                The initial rule of the grammar.
	 *
	 * @throws IllegalArgumentException
	 *                 If the rule is either not valid or not defined in the
	 *                 grammar.
	 */
	public void setInitialRule(String init) {
		if (init == null) {
			throw new NullPointerException("init must not be null");
		} else if (init.equals("")) {
			throw new IllegalArgumentException("The empty string is not a valid rule name");
		}

		initialRule = init;
	}

	/**
	 * Add an exported rule to this grammar.
	 *
	 * @param export
	 *                The name of the rule to export.
	 *
	 * @throws IllegalArgumentException
	 *                 If the rule is either not valid or not defined in the
	 *                 grammar.
	 */
	public void addExport(String export) {
		if (export == null) {
			throw new NullPointerException("Export name must not be null");
		} else if (export.equals("")) {
			throw new NullPointerException("The empty string is not a valid rule name");
		}

		exportedRules.add(export);
	}

	/**
	 * Suffix a given case element to every case of a specific rule.
	 *
	 * @param ruleName
	 *                The rule to suffix.
	 *
	 * @param suffix
	 *                The suffix to add.
	 *
	 * @throws IllegalArgumentException
	 *                 If the rule name is either invalid or not defined by
	 *                 this grammar, or if the suffix is invalid.
	 */
	public void suffixWith(String ruleName, String suffix) {
		if (ruleName == null) {
			throw new NullPointerException("Rule name must not be null");
		} else if (ruleName.equals("")) {
			throw new IllegalArgumentException("The empty string is not a valid rule name");
		}

		CaseElement element = CaseElement.createElement(suffix);

		for (RuleCase ruleCase : rules.get(ruleName).getCases()) {
			ruleCase.getElements().add(element);
		}
	}

	/**
	 * Prefix a given case element to every case of a specific rule.
	 *
	 * @param ruleName
	 *                The rule to prefix.
	 *
	 * @param prefix
	 *                The prefix to add.
	 *
	 * @throws IllegalArgumentException
	 *                 If the rule name is either invalid or not defined by
	 *                 this grammar, or if the prefix is invalid.
	 */
	public void prefixWith(String ruleName, String prefix) {
		if (ruleName == null) {
			throw new NullPointerException("Rule name must not be null");
		} else if (ruleName.equals("")) {
			throw new IllegalArgumentException("The empty string is not a valid rule name");
		}

		CaseElement element = CaseElement.createElement(prefix);

		for (RuleCase ruleCase : rules.get(ruleName).getCases()) {
			ruleCase.getElements().add(element);
		}
	}
}
